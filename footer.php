<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rara_Business
 */
    
    /**
     * After Content
     * 
     * @hooked rara_business_content_end - 20
    */
    do_action( 'rara_business_before_footer' );
    
    /**
     * Footer
     * 
     * @hooked rara_business_footer_start  - 20
     * @hooked rara_business_footer_top    - 30
     * @hooked rara_business_footer_bottom - 40
     * @hooked rara_business_footer_end    - 50
    */
    do_action( 'rara_business_footer' );
    
    /**
     * After Footer
     * 
     * @hooked rara_business_page_end    - 20
    */
    do_action( 'rara_business_after_footer' );
    
    wp_footer(); ?>
    <?php if(isset($_GET['filter'])) { ?>
        <script type="text/javascript">
            function makeFilterClick() {
                jQuery('button[data-filter$=<?= $_GET['filter']; ?>]').click();
            }
            jQuery( document ).ready(function() {
                makeFilterClick();
                setTimeout(makeFilterClick, 200);
                setTimeout(makeFilterClick, 500);
                setTimeout(makeFilterClick, 1000);
            });
        </script>
    <?php } ?>
    <?php if ( is_home() || is_front_page()) { ?>
        <script type="text/javascript">
            jQuery( document ).ready(function() {
                function updateLink() {
                    var name = jQuery('button.button.is-checked').attr('data-filter');
                    name = name.replace(".", "");
                    jQuery('#portfolio-link').attr('href', 'https://cm.perseus.mx/integraciones/?filter='+name);
                }
                jQuery('button.button').on('click', function() { 
                    updateLink();
                    setTimeout(updateLink, 200);
                    setTimeout(updateLink, 500);
                    setTimeout(updateLink, 1000);
                });
            });
        </script>
    <?php } ?>
</body>
</html>
